import os
import time
import copy

from checkers import Checkers, check_format


'''
В данном варианте шахмат реализованны пункты:
1) Реализовать чтение записи шахматной партии из выбранного пользователем файла в полной нотации. 
После чтения должна быть возможность двигаться вперед и назад по записи партии (с соответствующим изменением на поле). 
Должна быть возможность в выбранной позиции перейти из режима просмотра партии в обычный режим игры.(Сложность 2)
3) Реализовать возможность записи разыгрываемой шахматной партии в текстовый файл в полной нотации. 
Записанная партия должна корректно воспроизводиться в режиме чтения записи партии. (Сложность 2)
9) Реализовать возможность «отката» ходов. 
С помощью специальной команды можно возвращаться на ход (или заданное количество ходов) назад вплоть до начала партии. (Сложность 1)
'''


dafault_file = "match.txt"
default_field = ['8 r n b q k b n r 8', '7 p p p p p p p p 7', '6 . . . . . . . . 6', '5 . . . . . . . . 5',
		'4 . . . . . . . . 4', '3 . . . . . . . . 3', '2 P P P P P P P P 2', '1 R N B Q K B N R 1']

class Chess():

	def __init__(self):

		self.chet = 0
		self.color = { 0: 'белыми', 1: 'черными'}
		self.field = default_field
		self.lines = ['1', '2', '3', '4', '5', '6', '7', '8']
		self.rows = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
		self.moves = {}
		self.filemode = False
		self.filemoves = []

	# распечатать поле
	def print_field(self, message: str=None) -> str:
		print('\n  A B C D E F G H  ')
		print('					')
		for t in self.field:
			print(t)
		print('					')
		print('  A B C D E F G H  \n')

	# обновить поле после хода
	def update_field(self, list_field, cord_from: list, cord_to: list):
		figure_from = list_field[cord_from[0]][cord_from[1]]
		list_field[cord_to[0]][cord_to[1]] = figure_from
		list_field[cord_from[0]][cord_from[1]] = '.'
		i = 0
		list_my = []
		for t in reversed(list_field):
			str_inner = f'{8 - i} '
			for s in t:
				str_inner = str_inner + s + ' '
			str_inner = str_inner + f'{8 - i}'
			i += 1
			#print(str_inner)
			list_my.append(str_inner)
		self.field = copy.deepcopy(list_my)


	# запрос действий от пользователя
	def print_message(self, message: str) -> str:
		result = input(message)
		return result

	# конфертировать поле в список списков
	def field_to_list(self):
		copy_field = copy.deepcopy(self.field)
		list_my = []
		
		for t in reversed(copy_field):
			i = 0
			list_inner = []
			for s in t:
				if i % 2 == 0 and i != 0 and i != len(t) - 1:
					list_inner.append(s)
				i += 1
			list_my.append(list_inner)
		#print(list_my)
		return list_my
	
	# коныертировать ход в координаты
	def move_to_cord(self, move: str):
		list_from = []
		list_to = []
		for i in self.lines:
			if move[1] == i:
				list_from.append(int(i) - 1)
			if move[4] == i:
				list_to.append(int(i) - 1)
		for i in range(len(self.rows)):
			if move[0] == self.rows[i]:
				list_from.append(i)
			if move[3] == self.rows[i]:
				list_to.append(i)
		#print(list_from, list_to)
		return list_from, list_to
	
	# первый ход	
	def start(self):
		with open(f"{os.path.dirname(os.path.abspath(__file__))}/{dafault_file}", "w") as wfile:
			wfile.write('')

		self.print_field()
		if self.filemode == True:
			result = self.filemoves[0]
			print(f"Начинаем игру. Делаем ход {self.color[self.chet % 2]} по номером {self.chet + 1}: {result}")
			return result
		else:
			result = self.print_message("Начинаем игру. Сделайте ход белыми: ")
			return result

	# откатить ходы назад
	def return_move(self, move):
		count_my = move[7:]
		#print(count_my)
		try:
			count_my = int(count_my)
		except:
			result = self.print_message(f"После return должно быть указано число ходов на которое хотим откатить. Попробуйте сделать ход вновь :")
			return self.play(result)
		if count_my > self.chet:
			result = self.print_message(f"В этой игре еще не было сделано {count_my} ходов. Попробуйте сделать ход вновь :")
			return self.play(result)
		self.chet = self.chet - count_my
		self.field = self.moves[self.chet]
		#self.print_field()
		result = self.print_message(f"Партия возвращена к {self.chet + 1} ходу. Сделайте следующий ход {self.color[self.chet % 2]}:")
		return self.play(result)

	# загрузка ходов из файла
	def file_load(self, move):
		self.filemode = True
		self.chet = 0
		self.field = default_field
		file_name = move[5:]
		with open(f"{os.path.dirname(os.path.abspath(__file__))}/{file_name}", "r") as rfile:
			for line in rfile:
				dot = line.find('.')
				if dot != -1:
					line = line[dot + 1:]
				line = line.strip()
				lines  = line.split()
				for i in lines:
					#print(i)
					i = i.replace('×', '-').replace('x', '-').replace('?', '').replace('!', '').replace('#', '')
					#i = i.translate(None, '!?#')
					if i[0].istitle():
						#print(i)
						i = i[1:]
					self.filemoves.append(i)	
		#print(self.filemoves)
		#result = self.print_message(f"Партия была загружена из {file_name}")
		print(f"Файл с партией '{file_name}' был загружен\n")
		return self.play('start')

	def file_save(self, move, file_name=dafault_file):
		with open(f"{os.path.dirname(os.path.abspath(__file__))}/{file_name}", "a") as wfile:
			if self.chet == 1:
				move = f"{1 + self.chet//2}. {move} "
			elif self.chet % 2 != 0:
				move = f"\n{1 + self.chet//2}. {move} "
			wfile.write(move)


	
	# остальные ходы
	def play(self, move=None):


		# старт игры
		if self.chet == 0 and move == 'start':
			#print('here')
			move = self.start()
		# откат ходов
		if move.find('return') == 0:
			move = self.return_move(move)
		# из файла
		if move.find('file') == 0:
			move = self.file_load(move)
		# конец игры
		if move == 'end':
			#print("Партия была завершена")
			return 'end'


		# проверка формата сообщения от пользователя
		ch_f = check_format(move)
		if ch_f != 'ok':
			if self.filemode == True:
				print(ch_f)
				print(f"Была допущена ошибка в файле в блоке: {self.filemoves[self.chet]}. Игра будет прервана.")
				return self.play('end')
			else:
				result = self.print_message(ch_f)
				return self.play(result)
		list_field = self.field_to_list()
		cord_from, cord_to = self.move_to_cord(move)
		checker = Checkers(list_field, cord_from, cord_to, self.chet)
		# проверки корректности хода
		ch_m = checker.check_move()
		if ch_m != 'ok':
			if self.filemode == True:
				print(ch_m)
				print(f"Была допущена ошибка в файле в блоке: {self.filemoves[self.chet]}. Игра будет прервана.")
				return self.play('end')
			else:
				result = self.print_message(ch_m)
				return self.play(result)
		
		# обновляем данные на поле
		self.moves[self.chet] = self.field
		figure_from = list_field[cord_from[0]][cord_from[1]]
		self.update_field(list_field=list_field, cord_from=cord_from, cord_to=cord_to)
		self.print_field()
		self.chet += 1

		if figure_from not in ['p', 'P', '.']:
			move = figure_from.upper() + move
		self.file_save(move)

		# начинаем следующий ход
		if self.filemode == True:
			if len(self.filemoves) == self.chet:
				print(f"Данные из файла кончились")
				result = 'no'
			else:
				result = self.print_message(f"Переходим к следующему ходу(yes/no) или начнем игру?:")
			if result == 'yes':
				print(f"Ход из файла. Делаем ход {self.color[self.chet % 2]} по номером {self.chet + 1}: {self.filemoves[self.chet]}")
				return self.play(self.filemoves[self.chet])
			elif result == 'no':
				result = self.print_message(f"Сделайте следующий ход {self.color[self.chet % 2]} по номером {self.chet + 1}:")
				self.filemode = False
				return self.play(result)
			else:
				print(f"Вы ввели некорректное значение. Был Автоматически сделан следующий ход из файла: {self.filemoves[self.chet]}")
				return self.play(self.filemoves[self.chet])
		else:
			result = self.print_message(f"Сделайте следующий ход {self.color[self.chet % 2]} по номером {self.chet + 1}:")
			return self.play(result)
		

ch = Chess()
print(ch.play('start'))
#print(ch.play('file chess.txt'))