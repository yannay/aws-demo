import itertools
from math import pi, sin, cos, tan
from turtle import distance

from show import show

class Figure():

	def __init__(self, count: int = 10, start: int = 0, step: int = 2, hight: int = 0, size: int = 1):
		self.count = count
		self.start = start
		self.step = step
		self.hight = hight
		self.size = size

	def __gen(self, figure_gen):
		l1 = map(lambda x: figure_gen(x), itertools.count(self.start, self.step))
		i = 0
		list_my = []
		for t in l1:
			if i >= self.count:
				print(f"Сгенерированно {i} фигур")
				break
			else:
				i += 1
				list_my.append(t)
		return list_my

	
	# генерация четырехугольника
	def gen_rectangle(self) -> list:
		def __gen_rectangle(x) -> tuple:
			return ((x, self.hight), (x, self.size + self.hight) , (x + self.size, self.size + self.hight), (x + self.size, self.hight))
		list_my = self.__gen(__gen_rectangle)
		return list_my

	# генерация треугольника
	def gen_triangle(self) -> list:
		def __gen_triangle(x) -> tuple:
			return ((x, self.hight), (x + 1/2 * self.size, ((3 ** 0.5) / 2) * self.size + self.hight) , (x + self.size, self.hight))
		list_my = self.__gen(__gen_triangle)
		return list_my

	# генерация шестиугольника
	def gen_hexagon(self) -> list:
		def __gen_hexagon(x) -> tuple:
			return ((x, self.hight), (x - 1/2 * self.size, ((3 ** 0.5) / 2) * self.size + self.hight), (x, (3 ** 0.5) * self.size + self.hight), 
					(x + self.size, (3 ** 0.5) * self.size + self.hight), (x + 3/2 * self.size, ((3 ** 0.5) / 2) * self.size + self.hight) , (x + self.size, self.hight))
		list_my = self.__gen(__gen_hexagon)
		return list_my

	# параллельный перенос
	@staticmethod
	def tr_translate(figure: tuple, distance: int = 5) -> tuple:
		first = figure[0]
		last = figure[-1]
		sin_my = (last[1]-first[1]/(((last[1]-first[1]) ** 2 + (last[0]-first[0]) ** 2)) ** 0.5)
		cos_my = (last[0]-first[0]/(((last[1]-first[1]) ** 2 + (last[0]-first[0]) ** 2)) ** 0.5)
		figure_new = map(lambda x: (x[0] - distance * sin_my, x[1] + distance * cos_my), figure)
		return tuple(figure_new)

	# поворот
	@staticmethod
	def tr_rotate(figure: tuple, angle: int = 45, point: tuple = (0, 0)) -> tuple:
		sin_my = round(sin(pi*angle/180), 7)
		cos_my = round(cos(pi*angle/180), 7)
		figure_new = map(lambda x: (point[0] + (x[0] - point[0]) * cos_my - (x[1] - point[1]) * sin_my,
							point[1] + (x[0] - point[0]) * sin_my + (x[1] - point[1]) * cos_my), figure)
		return tuple(figure_new)

	# получить координаты центра фигуры
	@staticmethod
	def __get_center(figure: tuple) -> tuple:
		first = figure[0]
		last = figure[-1]
		angle_count = len(figure)
		angle = pi * (angle_count - 2)/(2 * angle_count)
		sin_my = round(sin(angle), 7)
		cos_my = round(cos(angle), 7)
		#print(cos_my)
		#print((last[0]- first[0])/((2*cos_my) ), (last[1]- first[1])/((2*cos_my) ))
		#print((last[0]- first[0]) * cos_my/(((cos_my))*2))
		x_center = first[0] + (last[0]- first[0]) * cos_my/((2*cos_my) ) - (last[1]- first[1]) * sin_my/((2*cos_my) )
		y_center = first[1] + (last[0]- first[0]) * sin_my/((2*cos_my) ) + (last[1]- first[1]) * cos_my/((2*cos_my))
		return (x_center, y_center)


	# симметрия
	@staticmethod
	def tr_symmetry(figure: tuple, distance: int = 5) -> tuple:
		figure_new_1 = Figure.tr_translate(figure=figure, distance=distance)
		point = Figure.__get_center(figure=figure_new_1)
		#print(point)
		figure_new_2 = Figure.tr_rotate(figure=figure_new_1, angle=180, point=point)
		return figure_new_2

	# получить параметры прямой
	def __get_line(vector: tuple, point: tuple) -> tuple():
		a = - vector[1]
		b = vector[0]
		c = point[0] * vector[1] - point[1] * vector[0]
		return a, b, c

	# знак
	@staticmethod
	def __sign(num: float) -> int:
		if (round(num, 7) == 0):
			return 0
		if num > 0:
			return 1
		elif num < 0: 
			return -1
		else:
			return 0 

	# гомотетия после параллельного переноса
	@staticmethod
	def __tr_homothety(coord: tuple, angle: int , point: tuple , vector: tuple, vector_n: tuple) -> tuple:
		#print(f"coord {coord}")
		a, b, c = Figure.__get_line(vector=vector_n, point=coord)
		distance = - (a * point[0] + b * point[1] + c)/(((a ** 2) + (b ** 2)) ** 0.5)
		#print(a, b, c)
		a, b, c = Figure.__get_line(vector=vector, point=point)
		sign = Figure.__sign(- (a * coord[0] + b * coord[1] + c)/(((a ** 2) + (b ** 2)) ** 0.5))
		#print(f"distance {distance}")
		#print(f"sign {sign}")
		distance_n = (distance) * round(tan(pi*angle/180), 7)
		#print(f"distance_n {distance_n}")
		point_new = (point[0] + vector[0] * (distance) + sign * vector_n[0] * distance_n,
					point[1] + vector[1] * (distance) + sign * vector_n[1] * distance_n)
		#print(point_new)
		return point_new
	
	# проверка наклона прямой
	@staticmethod
	def __check_line(figure: tuple, a: float, b: float, c: float) -> tuple:
		list_my = []
		i=0
		for point in figure:
			if round(point[0] * a + point[1] * b + c, 7) == 0:
				if i>=len(figure)/2:
					#list_my.append((point[0] - 0.02, point[1] - 0.03))
					list_my.append((point[0] + 0.02, point[1] + 0.03))
					list_my.append((point[0] - 0.02, point[1] - 0.03))
				else:
					list_my.append((point[0] - 0.02, point[1] - 0.03))
					list_my.append((point[0] + 0.02, point[1] + 0.03))
			else:
				list_my.append(point)
			i+=1
		return tuple(list_my)

	
	# гомотетия
	@staticmethod
	def tr_homothety(figure: tuple, angle: int = 45, point: tuple = (0, 0)) -> tuple:
		first = figure[0]
		last = figure[-1]
		vector = (last[0] - first[0]/((((last[0] - first[0]) ** 2) + ((last[1] - first[1]) ** 2)) ** 0.5),
				 last[1] - first[1]/((((last[0] - first[0]) ** 2) + ((last[1] - first[1]) ** 2)) ** 0.5))
		#print(f"vector {vector}")
		a, b, c = Figure.__get_line(vector=vector, point=point)
		center = Figure.__get_center(figure=figure)
		distance = - (a * center[0] + b * center[1] + c)/(((a ** 2) + (b ** 2)) ** 0.5)
		figure_new_1 = Figure.tr_translate(figure=figure, distance=distance)
		figure_new_2 = Figure.__check_line(figure=figure_new_1, a=a, b=b, c=c)
		#print(figure_new_2)
		vector_n = (last[1] - first[1]/((((last[0] - first[0]) ** 2) + ((last[1] - first[1]) ** 2)) ** 0.5), 
					first[0] - last[0]/((((last[0] - first[0]) ** 2) + ((last[1] - first[1]) ** 2)) ** 0.5))
		#print(f"vector_n {vector_n}")
		figure_new_3 = list(map(lambda x: Figure.__tr_homothety(
										coord=x, angle=angle, point=point, vector=vector, vector_n=vector_n), figure_new_2))
		#print(figure_new_3)
		return figure_new_3

figure = Figure(count=4, start=5, hight=3)
# 2.1
# rectangle = figure.gen_rectangle()
# show(rectangle)
# show(Figure(count=4, start=5, hight=3).gen_rectangle())

# 2.2
# show(Figure(count=2, start=2, hight=-2).gen_triangle())

# 2.3
# show(Figure(count=5, step=4, size=1.5).gen_hexagon())

# 2.4
# figure_7 = itertools.chain(Figure(count=5, step=4).gen_hexagon(), Figure(count=2, start=18).gen_triangle(),
# Figure(count=4, hight=2).gen_rectangle(), Figure(count=5,  start=8, step=4, hight=2).gen_hexagon(),
# Figure(count=2, hight=4).gen_triangle(), Figure(count=2, start=4, hight=4).gen_rectangle(), Figure(count=2, start=8, hight=4).gen_hexagon(),
# Figure(count=10, hight=6).gen_rectangle(), Figure(count=2, start=20, hight=6).gen_triangle(),
# Figure(count=3, hight=8).gen_triangle(), Figure(count=1, start=6, hight=8).gen_hexagon(), Figure(count=3, start=8, hight=8).gen_rectangle(),
# Figure(count=1, hight=10).gen_hexagon(), Figure(count=1, start=2, hight=10).gen_rectangle(), Figure(count=1, start=4, hight=10).gen_hexagon(), Figure(count=1, start=6, hight=10).gen_rectangle(),
# Figure(count=2, step=5, hight=12).gen_triangle(), Figure(count=1, start=10, hight=12).gen_rectangle())
# show(figure_7)

# 3.1
# translate_list = list(itertools.chain(Figure(count=5).gen_hexagon(),
# list(map(lambda x: figure.tr_translate(x), Figure(count=5).gen_hexagon()))))
# show(translate_list)

# 3.2
# rotate_list = list(itertools.chain(Figure(count=5, step=3, size=1.5).gen_hexagon(),
# list(map(lambda x: figure.tr_rotate(x), Figure(count=5, step=3, size=1.5).gen_hexagon()))))
# show(rotate_list)

# 3.3
# symmetry_list = list(itertools.chain(Figure(count=5).gen_rectangle(),
# list(map(lambda x: figure.tr_symmetry(x), Figure(count=5).gen_rectangle()))))
# show(symmetry_list)
# symmetry_list = list(itertools.chain(list(map(lambda x: figure.tr_rotate(x), Figure(count=5).gen_triangle())),
# list(map(lambda x: figure.tr_symmetry(x), list(map(lambda x: figure.tr_rotate(x), Figure(count=5).gen_triangle()))))))
# show(symmetry_list)

# 3.4
# homothety_list = list(itertools.chain(Figure(count=5, step=2.2).gen_hexagon(),
# list(map(lambda x: figure.tr_homothety(x,point=(5, 1)), Figure(count=5, step=2.2).gen_hexagon()))))
# show(homothety_list)
# homothety_list = list(itertools.chain(list(map(lambda x: figure.tr_rotate(x), Figure(count=2).gen_rectangle())),
# list(map(lambda x: figure.tr_homothety(x, point=(0, 3)), list(map(lambda x: figure.tr_rotate(x), Figure(count=2).gen_rectangle()))))))
# show(homothety_list)

# 4.1
# translate_list = list(itertools.chain(list(map(lambda x: figure.tr_rotate(x), Figure(count=10).gen_rectangle())),
# list(map(lambda x: figure.tr_translate(x, distance=5), list(map(lambda x: figure.tr_rotate(x), Figure(count=10).gen_rectangle())))),
# list(map(lambda x: figure.tr_translate(x, distance=-5), list(map(lambda x: figure.tr_rotate(x), Figure(count=10).gen_rectangle()))))))
# show(translate_list)

# 4.2
# rotate_list = list(itertools.chain(list(map(lambda x: figure.tr_translate(x), Figure().gen_hexagon())),
# list(map(lambda x: figure.tr_rotate(x, point=(5,0)), Figure().gen_hexagon()))))
# show(rotate_list)

# 4.3
# symmetry_list = list(itertools.chain(Figure().gen_triangle(),
# list(map(lambda x: figure.tr_symmetry(x), Figure().gen_triangle()))))
# show(symmetry_list)

# 4.4
# homothety_list = list(map(lambda x: figure.tr_homothety(x, angle=30), list(map(lambda x: figure.tr_rotate(x), Figure(count=5, start=1).gen_rectangle()))))
# homothety_list_2 = list(itertools.chain(list(map(lambda x: figure.tr_rotate(x, angle=180), homothety_list)),
# homothety_list))
# show(homothety_list_2)
