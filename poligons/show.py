import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon

# добавить систему координат
# размер многоугольника
def show(poligons_list: list = [((0, 0), (0, 1), (1, 1), (1, 0)), ((2, 0), (2, 1), (3, 0), (3, 1)), ((4, 0), (4, 1), (5, 0), (5, 1))]):
	fig, ax = plt.subplots()
	patches = []

	for i, xy in enumerate(poligons_list):
		polygon = Polygon(xy, True)
		patches.append(polygon)

	p = PatchCollection(patches, alpha=0.4)
	ax.add_collection(p)
	ax.grid(which='major',
        color = 'k')

	ax.axhline(y=0, color='k')   
	ax.axvline(x=0, color='k')
	# ax.set_xlabel('x')  # Add an x-label to the axes.
	# ax.set_ylabel('y')  # Add a y-label to the axes.
	#ax.set_position((0, 0, 10, 10))
	ax.set_xlim(-20, 20)
	ax.set_ylim(-20, 20)
	ax.axis('equal')
	#ax.axis('scaled')
	#ax.axis('tight')
	#ax.spines['right'].set_color('none')


	#plt.axis('scaled')
	plt.show()