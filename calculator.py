import re

'''
В данном варианте калькулятора реализованны пункты:
2) Реализовать поддержку десятичной дробной части до миллионных долей включительно. 
Реализовать корректный вывод информации о периодической десятичной дроби 
(период дроби вплоть до 4х десятичных знаков). (Сложность 3)
3) Реализовать текстовый калькулятор для выражения
из произвольного количества операций с учетом приоритета операций. (Сложность 3)
5) Добавить возможность использования отрицательных чисел. (Сложность 1)
10) Диагностировать ошибки: неправильную запись числа; неправильную последовательность чисел и операций;
(задание 3) неправильную последовательность чисел и операций. (Сложность 1)
'''

def calculator(str_my: str) -> str:
	print(f"\nИсходная строка: \n{str_my}\n")
	delimiters = ['минус минус', 'плюс минус', 'умножить на минус', 'минус', 'плюс', 'умножить на']

	# разбиваем на списки со знаками и с цифрами
	numbers = []
	signs = []
	while True:
		min_my = 100000000000
		s = -1
		for delimiter in delimiters:
			place = str_my.find(delimiter)
			if place != -1 and place < min_my:
				min_my = place
				s = delimiter
		if s == -1:
			numbers.append(str_my.strip())
			break
		signs.append(s.strip())
		# если первый минус
		if s == delimiters[3] and min_my == 0:
			numbers.append('ноль')
		else:
			numbers.append(str_my[:min_my - 1].strip())
		str_my = str_my[min_my + len(s) + 1:]

	print("После разбиения на списки с операциями и числами:\n")
	print(numbers)
	print(signs)

	if len(signs) + 1 != len(numbers):
		return "Количетсво операций не соответсвует количеству чисел"

	# парсим обычное число до 1000
	def parse_number(num: str) -> int:
		number_dict = {'двести' : 200, 'триста' :  300, 'четыреста' : 400,
		'пятьсот' : 500, 'шестьсот' : 600, 'семьсот' : 700, 'восемьсот' : 800, 'девятьсот' : 900,
		'одиннадцать' : 11, 'двенадцать' : 12, 'тринадцать' : 13, 'четырнадцать': 14,
		'пятнадцать' : 15, 'шестнадцать' : 16, 'семнадцать' : 17, 'восемнадцать' : 18,
		'девятнадцать' : 19, 'двадцать' : 20, 'тридцать' : 30, 'сорок' : 40, 'пятьдесят' : 50,
		'шестьдесят' : 60, 'семьдесят' : 70, 'восемьдесят' : 80, 'девяносто' : 90, 'сто' : 100, 
		'ноль' : 0, 'один' : 1, 'одна' : 1, 'два' : 2, 'две' : 2, 'три' : 3, 'четыре' : 4,
		'пять' : 5, 'шесть' : 6, 'семь' : 7, 'восемь' : 8, 'девять' : 9, 'десять' : 10}
		res = 0
		for i in range(0, 3):
			for key, value in number_dict.items():
				#place = num.find(key)
				place = num.startswith(key)
				if place == True:
				#if place != -1:
					try:
						if i != 0:
							assert len(str(res)) > len(str(value))
					except AssertionError:
						raise ValueError("Нарушен порядок цифр в числе")
					res = res + value
					break
			# каждый раз должен находить вхождение первого слова
			try:
				assert place == True #place == 0
			except AssertionError:
				raise ValueError("Допущена ошибка при вводе выражения")
			# если дошли до последнего слова
			if len(key) == len(num):
				break	
			else:
				num = num[len(key) + 1:]
		return res	

			
	# парсим тысячи
	def parse_thousand(num: str) -> int:
		res = 0
		thousand_list = ['тысяча', 'тысячи', 'тысяч']
		for th in thousand_list:
			place = num.find(th)
			if place != -1 :
				if place + len(th) != len(num):
					res = 1000 * parse_number(num[:place - 1]) + parse_number(num[place + len(th) + 1:])
				else:
					res = 1000 * parse_number(num[:place - 1])
				break
		if place == -1:
			res = parse_number(num)
		return res

	# парсим дробь до миллионной
	def parse_fraction(fraction: str) -> int:
		res = 0
		delimiters_fr = {10: 'десятых',100: 'сотых',10000: 'десятитысячных',
		100000: 'статысячных',1000: 'тысячных',1000000: 'миллионных'}
		for key, value in delimiters_fr.items():
			place = fraction.find(value)
			if place != -1:
				try:
					assert place + len(value) == len(fraction)
				except AssertionError:
					raise ValueError("После дроби не должно быть иных символов")
				res = parse_thousand(fraction[:place - 1])/key
				break
		if place == -1:
			res = parse_number(fraction)
		return res

	# парсим смешанное число
	def parse_all(num) -> int:
		#print (type(num))
		if type(num) is int or type(num) is float:			
			return num
		place = num.find(' и ')
		if place != -1:
			first = parse_number(num[:place])
			second = parse_fraction(num[place + 3:])
			try:
				assert abs(second) <= 1
			except AssertionError:
				raise ValueError("Дробная часть не может быть больше единицы")
			return first + second
		else:
			return parse_fraction(num)

	print("\nПосле преобразования строк в числа:\n")
	for t in numbers:
		print(parse_all(t))
	print(signs)
	
	# делаем все умножения
	for sign in range(len(signs)):
		if signs[sign] == delimiters[2]:
			res = parse_all(numbers[sign]) * (-1) * parse_all(numbers[sign + 1])
			numbers[sign] = 0
			numbers[sign + 1] = res
			s = 'плюс'
			if sign >= 1:
				if signs[sign - 1] == delimiters[3] or signs[sign - 1] == delimiters[1] or signs[sign - 1] == delimiters[0]:
					s = 'минус'
			signs[sign] = s
		elif signs[sign] == delimiters[5]:
			res = parse_all(numbers[sign]) * parse_all(numbers[sign + 1])
			numbers[sign] = 0
			numbers[sign + 1] = res
			s = 'плюс'
			if sign >= 1:
				if signs[sign - 1] == delimiters[3] or signs[sign - 1] == delimiters[1] or signs[sign - 1] == delimiters[0]:
					s = 'минус'
			signs[sign] = s

	print("\nПосле умножений:\n")
	for t in numbers:
		print(parse_all(t))
	print(signs)

	# делаем все сложения
	#res = 0
	for sign in range(len(signs)):
		if signs[sign] == delimiters[0] or signs[sign] == delimiters[4]:
			res = parse_all(numbers[sign]) + parse_all(numbers[sign + 1])
			numbers[sign + 1] = res
		elif signs[sign] == delimiters[1] or signs[sign] == delimiters[3]:
			res = parse_all(numbers[sign]) - parse_all(numbers[sign + 1])
			numbers[sign + 1] = res

	print("\nПосле сложений:\n")
	print(numbers)
	# for t in numbers:
	# 	print(parse_all(t))

	decision = str(parse_all(numbers[-1]))

	print(f"\nОтвет в числах:\n{decision}")

	# перевод числа в строку
	def str_number(s: str, male: bool = True) -> str:
		'''
		переводим число от 0 до 999999 в строку
		можно расширить до миллионов или миллиардов
		написать универсальный алгоритм для сколь угодно больших чисел представляется более сложным
		'''

		# получить единицы
		def get_units(u: str, male: bool = True) -> str:
			units_dict = {0 : '', 1 : 'один', 2 :'два',3 : 'три', 4 : 'четыре',
			5 : 'пять',6 : 'шесть',7 : 'семь',8 : 'восемь',9 : 'девять'}
			res = units_dict[int(u)]
			if male == False and u == '2':
				res = 'две'
			elif male == False and u == '1':
				res = 'одна'
			return res

		# получить дестяки
		def get_dozens(d: str, male: bool = True) -> str:
			dozens_dict = {0 : 'десять', 1 : 'одиннадцать', 2 :  'двенадцать', 3 : 'тринадцать', 4 : 'четырнадцать',
			5: 'пятнадцать', 6 : 'шестнадцать' , 7 : 'семнадцать', 8 :  'восемнадцать', 9 : 'девятнадцать'}
			dozens_dict_2 = {0 : '', 2 : 'двадцать', 3 : 'тридцать', 4 : 'сорок', 5 : 'пятьдесят',
			6 : 'шестьдесят', 7 : 'семьдесят', 8 : 'восемьдесят', 9 : 'девносто'}
			if d[0] == '1':
				return dozens_dict[int(d[1])]
			else:
				return dozens_dict_2[int(d[0])] + " " + get_units(d[1], male)
		
		# получить сотни
		def get_hundreds(h: str) -> str:
			hundreds_dict = {0 : '',1 : 'сто', 2 : 'двести', 3: 'триста', 4: 'четыреста',
			5 : 'пятьсот', 6 : 'шестьсот', 7 : 'семьсот', 8: 'восемьсот', 9 : 'девятьсот'}
			return hundreds_dict[int(h)] + " "

		# получить склонение тысячи
		def get_thousands(t: str) -> str:
			thousand = " тысяч"
			if t[-1] == '1':
				return thousand + "а "
			elif t[-1] == '2' or t[-1] == '3' or t[-1] == '4' and t[0] != '1':
				return thousand + "и "
			else:
				return thousand + " "
			

		res = 'ноль'

		if len(s) == 6:
			res = get_hundreds(s[0]) + get_dozens(s[1:3], male) + get_thousands(s[1:3]) + get_hundreds(s[3]) + get_dozens(s[4:], male)
		elif len(s) == 5:
			res = get_dozens(s[:2], male) + get_thousands(s[:2]) + get_hundreds(s[2]) + get_dozens(s[3:], male)
		elif len(s) == 4:
			res = get_units(s[0], male) + get_thousands(s[0]) + get_hundreds(s[1]) + get_dozens(s[2:], male)
		elif len(s) == 3:
			res = get_hundreds(s[0]) + get_dozens(s[1:], male)
		elif len(s) == 2:
			res = get_dozens(s, male)
		elif len(s) == 1:
			if s != '0':
				res = get_units(s, male)
		else:
			res = "Число больше 999999"

		return res

	# перевод дроби в строку с точностью до миллионных
	def str_fraction(f: str) -> str:
		# возвращаем период, если есть
		def period(f: str) -> str:
			p = ''
			pred = 0
			for i in range(1, 5):
				pred = 0
				p = f[len(f):len(f)+i]
				for j in range(len(f)+1, 0, -i):
					# print(f[j-1:j+i-1])
					if p == f[j-1:j+i-1]:
						pred +=1
					else:
						pred = 0
						p = f[j-1:j+i-1]
					if pred == 2:
						return p

		per_val = period(f)
		if per_val:
			f = f.rstrip(per_val)

		res = ' и '
		if len(f) == 6:
			res = res + str_number(f, male = False) + " миллионных"
		elif len(f) == 5:
			res = res + str_number(f, male = False) + " стотысячных"
		elif len(f) == 4:
			res = res + str_number(f, male = False) + " десятитысячных"
		elif len(f) == 3:
			res = res + str_number(f, male = False) + " тысячных"
		elif len(f) == 2:
			res = res + str_number(f, male = False) + " сотых"
		elif len(f) == 1:
			res = res + str_number(f, male = False) + " десятых"
		elif len(f) == 0:
			res = ''
		else:
			res = res + str_number(f[:6], male = False) + " миллионных"

		if per_val:
			res = res + f" и {str_number(per_val, male = False)} в периоде"

		return res
	
	# обрабатываем минус в начале
	result = ''
	if decision.find('-') != -1:
		result = 'минус '
		decision = decision[1:]

	# обрабатываем само число
	dot = decision.find('.')
	if dot != -1:
		result = result + str_number(decision[:dot]) + str_fraction(decision[dot + 1:])
	else:
		result = result + str_number(decision)

	print(f"\nОтвет в буквах:\n{result}")
	return result.strip()

assert calculator("триста тридцать три и сто пятнадцать тысяч двести сорок три миллионных плюс двадцать пять плюс минус двадцать пять плюс тринадцать \
минус триста тридцать три и сто пятнадцать тысяч миллионных плюс сто пятнадцать тысяч две миллионных умножить на ноль и двадцать семь сотых \
минус двести семь умножить на девятьсот") == "минус сто восемьдесят шесть тысяч двести восемьдесят шесть и девятьсот шестьдесят восемь тысяч семьсот  шесть миллионных"

assert calculator("минус два минус сто умножить на три плюс четыре") == 'минус двести девносто восемь'

assert calculator("триста тридцать две тысячи триста двадцать две миллионных плюс триста двадцать два") == 'триста двадцать два и триста тридцать две тысячи триста двадцать две миллионных'

assert calculator("семь плюс ноль") == "семь"

assert calculator("семь") == "семь"

assert calculator(" сто ") == "сто"

assert calculator("минус пять сотых умножить на ноль") == "ноль и ноль десятых"

assert calculator("сто одиннадцать тысячных умножить на четыре") == "ноль и четыре в периоде"

assert calculator("ноль и сто одиннадцать тысяч сто одиннадцать миллионных умножить на три") == "ноль и три в периоде"

assert calculator("ноль и сто двадцать одна тысяча двести двенадцать миллионных умножить на два") == "ноль и двадцать четыре в периоде"

assert calculator("девяносто девять умножить на девяносто девять умножить на девяносто девять") == "девятьсот семьдесят  тысяч двести девносто девять"

assert calculator("девяносто девять умножить на девяносто девять умножить на девяносто девять\
	умножить на девяносто девять умножить на девяносто девять") == "Число больше 999999"

assert calculator("минус один умножить на минус три плюс минус четыре минус минус два") == "один"

assert calculator("один умножить на минус три плюс минус четыре минус минус два") == "минус пять"

assert calculator("девять     плюс     десять") == "девятнадцать"

#calculator("три тысячи двести один плюс один") # вернет ошибку
#calculator("привет") # вернет ошибку
#calculator(" ") # вернет ошибку
#calculator("") # вернет ошибку
#calculator("шесть и шесть") # вернет ошибку
#calculator("семь девяносто") # вернет ошибку
#calculator("семь шесть") # вернет ошибку
#calculator("тысяча сотых") # вернет ошибку
#calculator(" и сотых") # вернет ошибку
#calculator("три сотых три") # вернет ошибку