import sys
import random

def bulls_and_cows():
	'''
    Для работы с программой необходимо выполнить в терминале команду python3 bulls_and_cows.py
	После 100 неудачных вводов игроку засчитывается проигрыш, дабы не делать бесконечный цикл
	'''
	
	numbers = 4 # количество знаков в числе
	iterations = 100 # количество попыток для игрока
	list_my = generate_number(numbers)
	print(list_my)
	for t in range(iterations):
		number = input("Введите четырехзначное число без повторения цифр: ")
		list_player = [int(x) for x in number]
		if len(list_player) != numbers:
			print ("Число должно быть четырехзначным")
			continue
		set_player =set(list_player)
		if len(set_player) != numbers:
			print ("В числе не могут повторяться цифры")
			continue
		bull = 0
		cow = 0
		for i in range(len(list_player)):
			for j in range(len(list_my)):
				if list_player[i] == list_my[j]:
					if i == j:
						bull += 1
					else:
						cow += 1
					#print (i, j)
		print(f"Количество быков: {bull}")
		print(f"Количество коров: {cow}")
		if  bull == numbers:
			print("Это правильный ответ")
			break
	#print(t)
	if t == iterations - 1:
		print("Похоже вы проиграли")

def generate_number(num_my: int):
	guesses = list(range(10))
	random.shuffle(guesses)
	del guesses[num_my-10:]
	# print(guesses)
	return guesses

bulls_and_cows()