import os
import csv
import copy
import pickle

'''
В данном варианте задания с таблицы реализованны пункты:
4) Реализовать автоматическое определение типа столбцов по хранящимся в таблице значениям. 
Оформить как отдельную функцию и встроить этот функционал как опцию работы функции load_table.(Сложность 1 или 2)
3) Реализовать функцию merge_tables(table1, table2, by_number=True): в результате слияния создается таблица с набором столбцов, 
соответствующих объединенному набору столбцов исходных таблиц. Соответствие строк ищется либо по их номеру (by_number=True) 
либо по значению индекса (1й столбец). (Сложность 2)
9) Реализовать полноценную поддержку значения None в незаполненных ячейках таблицы. 
Должно работать при загрузке ячеек с пропусками значений, при операциях приводящих к появлению пустых ячеек, 
при работе с get и set операциями. (Сложность 1 или 2)
'''
class Table:

	def __init__(self, table_name: str):
		self.table = {}
		self.types = {}
		self.table_name = table_name
		self.table_path = f"{os.path.dirname(os.path.abspath(__file__))}/{table_name}"

	# декоратор проверяет была ли загружена таблица
	def __check_empty(f):
		def func(self, *args, **kwargs):
			if self.table != {}:
				return f(self, *args, **kwargs)
			else:
				raise ValueError(f"Необходимо загрузить таблицу {self.table_name}")
		return func

	# декоратор проверяет была ли заданы типы
	def __check_types(f):
		def func(self, *args, **kwargs):
			if self.types != {}:
				return f(self, *args, **kwargs)
			else:
				raise ValueError(f"Необходимо задать типы стобцов для таблицы {self.table_name}")
		return func

	# переводит значение к нужному типу
	def __value_to_type(self, value, type_my):
		try:
			if type_my is int:
				value_my = int(value)
			elif type_my is float:
				value_my = float(value)
			elif type_my is bool:
				value_my = bool(value)
			else:
				value_my = str(value)
		except:
			raise ValueError(f"Значение {value} нельзя привести к типу {type_my}")
		return value_my

	# получаем тип значения
	def __get_type(self, value):
		if value is True or value is False or value == 'True' or value == 'False':
			return bool
		else:
			try:
				float(value)
				value = str(value)
				if '.' in value: 
					return float
				else: 
					return int
			except (ValueError, TypeError):
				return str


	# переводит все элементы списка к нужному типу
	def __list_to_type(self, values: list, type_my) -> list:
		list_new = []
		for t in values:
			value_my = self.__value_to_type(t, type_my)
			list_new.append(value_my)
		return list_new

	# загрузка таблицы
	def load_table(self, define_types=False) -> dict:
		def load_csv(define_types=False) -> dict:
			with open(self.table_path, newline='') as csvfile:
				spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
				i = 0
				dict_my = {}
				list_my = []
				for row in spamreader:
					try:
						if i == 0:
							for t in row:
								dict_my[t] = []
								list_my.append(t)
						else:
							for j in range(len(list_my)):
								list_inner = dict_my[list_my[j]]
								if row[j] == '':
									list_inner.append(None)
								else:
									#print(type(row[j]))
									#print(row[j])
									list_inner.append(row[j])
								dict_my[list_my[j]] = list_inner
					except:
						raise ValueError(f"Некорректный формат файла: {self.table_name}")
					finally:
						i += 1
					#print(row)
					#print(', '.join(spamreader[row]))
				print(f"Была загружена таблица:\n{dict_my}\n")
				self.table = dict_my
				if define_types == True:
					self.define_column_types()
				return dict_my

		def load_pickle(define_types=False) -> dict:
			with open(self.table_path, 'rb') as f:
				reader = pickle.load(f)
				print(f"Была загружена таблица:\n{reader}\n")
				return reader

		
		if self.table_name.find('.csv') != -1 or self.table_name.find('.txt') != -1:
			return load_csv(define_types)
		elif self.table_name.find('.pickle') != -1 or self.table_name.find('.pkl') != -1:
			return load_pickle(define_types)
		else:
			raise ValueError(f"Некорректное разрешение файла: {self.table_name}")


	# получение таблицы по номерам строк
	@__check_empty
	def get_rows_by_number(self, start: int = 0, stop: int = 0, copy_table=False):
		if stop == 0:
			stop = start
		if stop < start:
			raise ValueError(f"Номер первой строки {start} не может быть больше номер последней {stop}")
		if stop < 0 or start < 0:
			raise ValueError(f"Номера строк {start} {stop} не могут быть отрицательными")

		dict_my= {}	
		if copy_table == True:
			dict_my = copy.deepcopy(self.table)
		elif copy_table == False:
			dict_my = self.table
		else:
			raise ValueError(f"Некорректно задан copy_table: {copy_table}")

		for key, value in dict_my.items():
			list_inner = []
			for i in range(start, stop + 1):
				if i < len(value):
					list_inner.append(value[i])
				else:
					#print("Окончание отрезка не может превышать количество записей в таблице")
					break
			dict_my[key] = list_inner
		print(f"По номерам строк {start} {stop} была получена таблица:\n{dict_my}")
		print(f"Исходная таблица выглядит так:\n{self.table}\n")
		return dict_my

	# получение таблицы по значениям из первого столбца
	@__check_empty
	def get_rows_by_index(self, *args, copy_table=False):
		dict_my= {}	
		if copy_table == True:
			dict_my = copy.deepcopy(self.table)
		elif copy_table == False:
			dict_my = self.table
		else:
			raise ValueError(f"Некорректно задан copy_table: {copy_table}")

		# получаем номера необходимых строк
		list_my = []
		len_my = 0
		for key, value in dict_my.items():
			for a in args:
				for t in range(len(value)):
					if a == value[t] and t not in list_my:
						list_my.append(t)
						break
				if t == len(value) - 1:
					raise ValueError(f"Значения {a} нет в первом столбце, либо оно уже было добавлено")
			len_my = len(value)
			break

		# получаем все строки по ранее найденным номерам
		for key, value in dict_my.items():
			list_inner = []
			for i in range(len_my):
				if i in list_my:
					list_inner.append(value[i])
				# else:
				# 	break
			dict_my[key] = list_inner
		
		print(f"По значениям из первого столбца:\n{args}\nбыла получена таблица:\n{dict_my}")
		print(f"Исходная таблица выглядит так:\n{self.table}\n")
		return(dict_my)

	# получение словаря с типами столбцов
	@__check_empty
	@__check_types
	def get_column_types(self, by_number=True):
		if by_number == True:
			dict_my = self.types
			dict_types = {}
			i = 0
			for key, value in dict_my.items():
				dict_types[i] = value
				i += 1
			print(f"Получили словарь типов:\n{dict_types}\n")
			return dict_types
		elif by_number == False:
			print(f"Получили словарь типов:\n{self.types}\n")
			return self.types
		else:
			raise ValueError(f"Некорректно задан by_number: {by_number}")


	# задание словаря с типами столбцов
	@__check_empty
	def set_column_types(self, types_dict, by_number=True):
		dict_my = self.table
		# if len(types_dict) != len(dict_my):
		# 	raise ValueError("Количетсво столбцов в словаре с типами не свопадает с количетсвом столбцов в таблице")

		# если в словаре нет нужного ключа, столбцу будет задан тип str
		dict_types = {}
		try:
			if by_number == True:
				i = 0
				for key in dict_my:
					if i in types_dict:
						dict_types[key] = types_dict[i]
					else:
						dict_types[key] = str
					i += 1
				# for key, value in types_dict.items():
				# 	if type(key) is not int:
				# 		raise ValueError("Ключи словаря должны быть целыми числами для заданного by_number")
				# 	dict_types[list(dict_my.keys())[key]] = value
				self.types = dict_types
			elif by_number == False:
				for key in dict_my:
					if key in types_dict:
						dict_types[key] = types_dict[key]
					else:
						dict_types[key] = str
				self.types = dict_types
			else:
				raise ValueError(f"Некорректно задан by_number: {by_number}")
		except:
			raise ValueError("Передан некорректный словарь")
		print(f"Был задан словарь типов:\n{self.types}\n")


	# возвращаем столбец приведенный к типу
	@__check_empty
	@__check_types
	def get_values(self, column=0):
		dict_my = self.table
		key_my = column
		try:
			if type(column) is int and column >= 0:
				key_my = list(dict_my.keys())[column]
			else:
				assert key_my in dict_my
		except:
			raise ValueError(f"Указанный столбец {key_my} не найден в таблице")

		type_my = self.types[key_my]

		#print(key_my)
		#print(type_my)

		# переводим в нужный тип
		list_new = self.__list_to_type(dict_my[key_my], type_my)
		
		print(f"Получен столбец приведенный к типу:\n{list_new}\n")
		return list_new

	# возвращаем столбец приведенный к типу для таблицы из одной строки
	@__check_empty
	@__check_types
	def get_value(self, column=0):
		dict_my = self.table
		key_my = column
		try:
			if type(column) is int and column >= 0:
				key_my = list(dict_my.keys())[column]
			else:
				assert key_my in dict_my
		except:
			raise ValueError(f"Указанный столбец {key_my} не найден в таблице")
		
		# проверяем, что у таблицы одна строка
		if len(dict_my[key_my]) != 1:
			print(len(dict_my[key_my]))
			raise ValueError(f"В таблице {self.table_name} длолжна быть одна строка для работы этого метода")
	
		type_my = self.types[key_my]

		# переводим в нужный тип
		value_my = self.__value_to_type(value=dict_my[key_my][0], type_my=type_my)
		
		print(f"Получен однострочный столбец приведенный к типу:\n{value_my}\n")
		return value_my

	# приводим значения к типу и записываем в таблицу
	@__check_empty
	@__check_types
	def set_values(self, values: dict, column=0):
		dict_my = self.table
		key_my = column
		try:
			if type(column) is int and column >= 0:
				key_my = list(dict_my.keys())[column]
			else:
				assert key_my in dict_my
		except:
			raise ValueError(f"Указанный столбец {key_my} не найден в таблице")

		type_my = self.types[key_my]

		if len(values) > len(dict_my[key_my]):
			raise ValueError(f"Длина столбца {key_my} меньше длины переданного списка")
		
		# добавляем зачения из столбца
		list_new = copy.deepcopy(values)
		for i in range(len(values), len(dict_my[key_my])):
			list_new.append(dict_my[key_my][i])

		#print(list_new)	
		# переводим в нужный тип
		list_new = self.__list_to_type(list_new, type_my)
		#print(list_new)	

		dict_my[key_my] = list_new
		print(f"После записи столбца:\n{values}\nтаблица приняла вид:\n{self.table}\n")

	# приводим значения к типу и записываем в таблицу из одной строки
	@__check_empty
	@__check_types
	def set_value(self, value, column=0):
		dict_my = self.table
		key_my = column
		try:
			if type(column) is int and column >= 0:
				key_my = list(dict_my.keys())[column]
			else:
				assert key_my in dict_my
		except:
			raise ValueError(f"Указанный столбец {key_my} не найден в таблице")

		type_my = self.types[key_my]

		# проверяем, что у таблицы одна строка
		if len(dict_my[key_my]) != 1:
			print(len(dict_my[key_my]))
			raise ValueError(f"В таблице {self.table_name} длолжна быть одна строка для работы этого метода")

		# переводим в нужный тип
		value_my = self.__value_to_type(value=value, type_my=type_my)
		#print(value_my)

		#value_my = None if value_my == 'None' else value_my

		dict_my[key_my][0] = value_my
		print(f"После записи однострочного столбца:\n{value}\nтаблица приняла вид:\n{self.table}\n")
	
	# определить типы столбцов таблиц по заданным в них значениям
	@__check_empty
	def define_column_types(self):
		dict_my = copy.deepcopy(self.table)
		dict_types = {}
		for key, value in dict_my.items():
			type_int = 0
			type_float = 0
			type_str = 0
			type_bool = 0
			type_column = str
			# считаем сколько каких типов есть в столбце
			for v in value:
				type_my = self.__get_type(v)
				if type_my is int:
					type_int +=1
				elif type_my is float:
					type_float +=1
				elif type_my is bool:
					type_bool +=1
				else:
					type_str +=1
			#print(type_int, type_float, type_bool, type_str)

			# считаем итоговый тип столбца
			if type_bool > 0 or type_str > 0:
				if type_str >= type_bool:
					type_column = str
				else:
					type_column = bool
			else:
				if type_float >= type_int:
					type_column = float
				else:
					type_column = int
			dict_types[key] = type_column

		print(f"Основывась на значениях таблицы был задан словарь типов:\n{dict_types}\n")
		self.types = dict_types



	def merge_tables(self, table1, table2, by_number=True):
		# проверка на что нет столбцов с одинаковыми именами
		for key1 in table1.table:
			for key2 in table2.table:
				if key1 == key2:
					raise ValueError(f"В таблицах {table1.table_name} и {table2.table_name} есть повторяющийся стобец: {key1}")

		# копируем таблицы Сначала меньшую Потом большую
		diff = len(list(table1.table.values())[0]) - len(list(table2.table.values())[0])
		#print(diff)
		dict_my1 = {}
		dict_my2 = {}
		if diff >= 0:
			dict_my1 = copy.deepcopy(table2.table)
			dict_my2 = copy.deepcopy(table1.table)
		else:
			dict_my1 = copy.deepcopy(table1.table)
			dict_my2 = copy.deepcopy(table2.table)

		# заполняем меньшую таблицу пустыми строками
		if by_number == True:
			for key, value in dict_my1.items():
				for i in range(diff):
					value.append(None)
		# случай когда нужно смержить по совпадениям в первых столбцах			
		elif by_number == False:
			list_my1 = list(dict_my1.values())[0]
			list_my2 = list(dict_my2.values())[0]
			if len(set(list_my1)) != len(list_my1) or len(set(list_my2)) != len(list_my2):
				raise ValueError(f"В первых стобцах таблиц {table1.table_name} и {table2.table_name} не может быть повторений\
					По ним будет происходить джоин")
			dict_index = {}
			list_index1 = []
			list_index2 = []
			for i in range(len(list_my1)):
				for j in range(len(list_my2)):
					if list_my1[i] == list_my2[j]:
						dict_index[j] = i
						break
					if j == len(list_my2) - 1:
						list_index1.append(i)
			for j in range(len(list_my2)):
				if j not in dict_index:
					list_index2.append(j)

			# print(dict_index)
			# print(list_index1)
			# print(list_index2)

			# добавляем пустые строчки к первой таблице
			for key, value in dict_my1.items():
				for i in range(len(list_index2)):
					value.append(None)

			#print(dict_my1)

			# добавляем пустые строчки ко второй таблице
			for key, value in dict_my2.items():
				for i in range(len(list_index1)):
					value.append(None)

			#print(dict_my2)
			
			# переставляем строчки в большей таблице в соответствии с первой
			for key, value in dict_my2.items():
				dict_new = {}
				len_my = len(list(dict_my1.values())[0])
				j = len(list_index2)
				k = 0
				for i in range(len(value)):
					if i in dict_index:
						dict_new[dict_index[i]] = value[i]
					elif i in list_index2:
						dict_new[len_my - j] = value[i]
						j -= 1
					else:
						dict_new[list_index1[k]] = value[i]
						k += 1
				#print(dict_new)
				list_new = []
				for k in sorted(dict_new):
					list_new.append(dict_new[k])
				dict_my2[key] = list_new

			#print(dict_my1)
			#print(dict_my2)
		else:
			raise ValueError(f"Некорректно задан by_number: {by_number}")


		dict_my1.update(dict_my2)
		self.table = dict_my1
		print(f"В результате слияния таблицы:\n{table1.table}\nи таблицы:\n{table2.table}\nбыла получена таблица:\n{self.table}\n")
		return dict_my1
				

	
	# выгрузка таблицы в файл
	def save_table(self, dict_my: dict):
		def save_csv(dict_my: dict):
			with open(self.table_path, 'w', newline='') as csvfile:
				spamwriter = csv.writer(csvfile, delimiter=';', quotechar='|')
				list_my = []
				max_my = 0
				try:
					for key, value in dict_my.items():
						if max_my < len(value):
							max_my = len(value)
						list_my.append(key)
					spamwriter.writerow(list_my)
					for i in range(max_my):
						list_inner = []
						for key, value in dict_my.items():
							if i < len(value):
								if value[i] == None:
									list_inner.append('')
								else:
									list_inner.append(value[i])
								#list_inner.append(value[i])
						spamwriter.writerow(list_inner)
				except:
					raise ValueError('Некорректный формат словаря')

		def save_pickle(dict_my: dict):
			with open(self.table_path, 'wb') as f:
				pickle.dump(dict_my, f)
		
		if self.table_name.find('.csv') != -1 or self.table_name.find('.txt') != -1:
			print(f"Таблица записана в: {self.table_name}\n")
			return save_csv(dict_my)
		elif self.table_name.find('.pickle') != -1 or self.table_name.find('.pkl') != -1:
			print(f"Таблица записана в: {self.table_name}\n")
			return save_pickle(dict_my)
		else:
			raise ValueError(f"Некорректное разрешение файла: {self.table_name}")

	# печать страницы
	def print_table(self):
		with open(self.table_path, newline='') as csvfile:
			spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
			print(f"Вывод таблицы {self.table_name}:")
			for row in spamreader:
				print(', '.join(row))
		print('')



#table1 = Table('load.csv')
#able1.load_table()
#assert table1.load_table() == {'name': ['name0', 'name1', 'name2', 'name3'], 'age': ['age0', 'age1', 'age2', 'age3'], 'male': ['male0', 'male1', 'male2', 'male3']}
#assert table1.get_rows_by_number(start=1,stop=3,copy_table=False) == {'name': ['name1', 'name2', 'name3'], 'age': ['age1', 'age2', 'age3'], 'male': ['male1', 'male2', 'male3']}
# assert table1.get_rows_by_number(start=0,stop=0,copy_table=True) == {'name': ['name0'], 'age': ['age0'], 'male': ['male0']}
# assert table1.get_rows_by_number(start=2,stop=7,copy_table=True) == {'name': ['name2', 'name3'], 'age': ['age2', 'age3'], 'male': ['male2', 'male3']}
# assert table1.get_rows_by_index('name1', 'name2', copy_table=True) == {'name': ['name1', 'name2'], 'age': ['age1', 'age2'], 'male': ['male1', 'male2']}
# table1.set_column_types({5: int})
# assert str(table1.types) == "{'name': <class 'str'>, 'age': <class 'str'>, 'male': <class 'str'>}"
# table1.set_column_types({1: int, 'age': bool},by_number=True)
# assert str(table1.types) == "{'name': <class 'str'>, 'age': <class 'int'>, 'male': <class 'str'>}"
# table1.set_column_types({1: int, 'age': bool},by_number=False)
# assert str(table1.types) == "{'name': <class 'str'>, 'age': <class 'bool'>, 'male': <class 'str'>}"
# assert str(table1.get_column_types(by_number=True)) == "{0: <class 'str'>, 1: <class 'bool'>, 2: <class 'str'>}"
# assert str(table1.get_column_types(by_number=False)) == "{'name': <class 'str'>, 'age': <class 'bool'>, 'male': <class 'str'>}"
# assert table1.get_values(column=1) == [True, True, True, True]
# assert table1.get_values(column=0) == ['name0', 'name1', 'name2', 'name3']
# table1.set_values([2, 3, 4], column="name")
# assert table1.table == {'name': ['2', '3', '4', 'name3'], 'age': ['age0', 'age1', 'age2', 'age3'], 'male': ['male0', 'male1', 'male2', 'male3']}
# table1.set_values([True, 2, "age", 2.36], column=2)
# assert table1.table == {'name': ['2', '3', '4', 'name3'], 'age': ['age0', 'age1', 'age2', 'age3'], 'male': ['True', '2', 'age', '2.36']}
# table1.print_table()


# table2 = Table('one_line.csv')
# assert table2.load_table() == {'row0': ['1'], 'row1': ['True'], 'row2': ['hi'], 'row3': [None], 'row4': ['1.67']}
# table2.set_column_types({1: int, 'row0': float}, by_number=False)
# assert str(table2.types) == "{'row0': <class 'float'>, 'row1': <class 'str'>, 'row2': <class 'str'>, 'row3': <class 'str'>, 'row4': <class 'str'>}"
# assert table2.get_value(column=0) == 1.0
# table2.set_value(value=34.5, column=0)
# assert table2.table == {'row0': [34.5], 'row1': ['True'], 'row2': ['hi'], 'row3': [None], 'row4': ['1.67']}
# table2.set_value(value=None, column=2)
# assert table2.table == {'row0': [34.5], 'row1': ['True'], 'row2': ['None'], 'row3': [None], 'row4': ['1.67']}

# table3 = Table('names.csv')
# assert table3.load_table() == {'namex': ['name1', 'name2', 'name7', 'name8'], 'agex': ['age1', 'age2', 'age7', 'age8'], 'malex': ['male1', 'male2', 'male7', 'male8']}

# table4 = Table('merge.csv')
# table4_1 = Table('merge1.csv')
# table4_2 = Table('merge2.csv')
# table4_1.load_table()
# table4_2.load_table()
# table4.merge_tables(table4_1, table4_2, by_number=False)
# table4.save_table(table4.table)
# assert table4.table == {'namex': ['name1', 'name2', 'name7', 'name8', None, None], 'agex': ['age1', 'age2', 'age7', 'age8', None, None], 'malex': ['male1', 'male2', 'male7', 'male8', None, None], 'name': ['name1', 'name2', None, None, 'name0', 'name3'], 'age': ['age1', 'age2', None, None, 'age0', 'age3'], 'male': ['male1', 'male2', None, None, 'male0', 'male3']}
# table4.merge_tables(table4_1, table2)
# assert table4.table == {'row0': [34.5, None, None, None], 'row1': ['True', None, None, None], 'row2': ['None', None, None, None], 'row3': [None, None, None, None], 'row4': ['1.67', None, None, None], 'name': ['name0', 'name1', 'name2', 'name3'], 'age': ['age0', 'age1', 'age2', 'age3'], 'male': ['male0', 'male1', 'male2', 'male3']}

#table5 = Table('types.csv')
#assert table5.load_table(define_types=True) == {'name': ['0', 'name1', '2', 'name3'], 'age': ['age0', 'True', 'age2', 'False'], 'male': ['1.1', 'male1', 'male2', '3.4'], 'color': ['1', '1.1', '2.3', 'True'], 'time': ['2.3', '9.0', '7', '8']}
# #table5.define_column_types()
# assert str(table5.get_column_types()) == "{0: <class 'str'>, 1: <class 'str'>, 2: <class 'str'>, 3: <class 'bool'>, 4: <class 'float'>}"

#table6 = Table('pc.pickle')
#table6.save_table({'name': ['name1', 'name2'], 'age': ['age1', 'age2'], 'male': ['male1', 'male2']})
# assert table6.load_table() == {'name': ['name1', 'name2'], 'age': ['age1', 'age2'], 'male': ['male1', 'male2']}

#table7 = Table('save2.csv')
#table7.save_table({'name': ['Виктор', 'Алексей'], 'age': ['30', '25'], 'male': ['male', 'male']})
# #print(table7.load_table())
# assert table7.load_table() == {'name': ['name1', 'name2'], 'age': ['age1', 'age2'], 'male': ['male1', 'male2']}

# table8 = Table('empty.csv')
# table8.load_table()

# негативные тесты бросают Exception поэтому закомментированны

# table1.get_rows_by_number(start=-1,stop=0,copy_table=True) # отрицатательные строки
# table4.get_column_types(by_number=False) # незаданные типы столбцов
# table1.get_values(column=5) # несуществующий стобец
# table1.get_value(column=1) # get_value для многострочной таблицы
# table1.set_values([1, 2, 3, 4, 5, 6, 7], column=1) # длина списка больше длины таблицы
# table2.set_value(value=None, column=0) # перевод None во float
# table4.merge_tables(table4_1, table1, by_number=False) # столбцы с одинаковыми именами
