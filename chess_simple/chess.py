import os
import time
import copy

from checkers import check_format, check_move


'''
В данном варианте шахмат реализованны пункты:
1) Реализовать чтение записи шахматной партии из выбранного пользователем файла в полной нотации. 
После чтения должна быть возможность двигаться вперед и назад по записи партии (с соответствующим изменением на поле). 
Должна быть возможность в выбранной позиции перейти из режима просмотра партии в обычный режим игры.(Сложность 2)
3) Реализовать возможность записи разыгрываемой шахматной партии в текстовый файл в полной нотации. 
Записанная партия должна корректно воспроизводиться в режиме чтения записи партии. (Сложность 2)
9) Реализовать возможность «отката» ходов. 
С помощью специальной команды можно возвращаться на ход (или заданное количество ходов) назад вплоть до начала партии. (Сложность 1)
'''


dafault_file = "match.txt"
default_field = ['8 r n b q k b n r 8', '7 p p p p p p p p 7', '6 . . . . . . . . 6', '5 . . . . . . . . 5',
		'4 . . . . . . . . 4', '3 . . . . . . . . 3', '2 P P P P P P P P 2', '1 R N B Q K B N R 1']

#class Chess():

# def __init__(self):
# 	self.chet = 0
# 	self.color = { 0: 'белыми', 1: 'черными'}
# 	self.field = default_field
# 	self.lines = ['1', '2', '3', '4', '5', '6', '7', '8']
# 	self.rows = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
# 	self.moves = {}
# 	self.moves_file = {}
# 	self.filemode = False
# 	self.filemoves = []
# 	self.mate = False
# распечатать поле
def print_field(field, message: str=None) -> str:
	print('\n  A B C D E F G H  ')
	print('					')
	for t in field:
		print(t)
	print('					')
	print('  A B C D E F G H  \n')
# есть возврат
# обновить поле после хода
def update_field(field, list_field, cord_from: list, cord_to: list):
	figure_from = list_field[cord_from[0]][cord_from[1]]
	list_field[cord_to[0]][cord_to[1]] = figure_from
	list_field[cord_from[0]][cord_from[1]] = '.'
	i = 0
	list_my = []
	for t in reversed(list_field):
		str_inner = f'{8 - i} '
		for s in t:
			str_inner = str_inner + s + ' '
		str_inner = str_inner + f'{8 - i}'
		i += 1
		#print(str_inner)
		list_my.append(str_inner)
	field = copy.deepcopy(list_my)
	return field
# запрос действий от пользователя
def print_message(message: str) -> str:
	result = input(message)
	return result
# конфертировать поле в список списков
def field_to_list(field):
	copy_field = copy.deepcopy(field)
	list_my = []
	
	for t in reversed(copy_field):
		i = 0
		list_inner = []
		for s in t:
			if i % 2 == 0 and i != 0 and i != len(t) - 1:
				list_inner.append(s)
			i += 1
		list_my.append(list_inner)
	#print(list_my)
	return list_my

# конвертировать ход в координаты
def move_to_cord(lines, rows, move: str):
	list_from = []
	list_to = []
	for i in lines:
		if move[1] == i:
			list_from.append(int(i) - 1)
		if move[4] == i:
			list_to.append(int(i) - 1)
	for i in range(len(rows)):
		if move[0] == rows[i]:
			list_from.append(i)
		if move[3] == rows[i]:
			list_to.append(i)
	#print(list_from, list_to)
	return list_from, list_to

# первый ход	
def start(chet, color, field, moves_file, filemode, filemoves):
	with open(f"{os.path.dirname(os.path.abspath(__file__))}/{dafault_file}", "w") as wfile:
		wfile.write('')
	moves_file[0] = ''
	print('\nСписок основных команд:\n\n',
		'end - прекращение партии\n',
		'return x - возвращает партию на x ходов назад (x задает пользователь)\n',
		'file x.txt - пошагово считывает партию из файлика x.txt (x.txt задает пользователь)\n'
		)
	print_field(field)
	if filemode == True:
		result = filemoves[0]
		print(f"Начинаем игру. Делаем ход {color[chet % 2]} по номером {chet + 1}: {result}")
		return result
	else:
		result = print_message("Начинаем игру. Сделайте ход белыми: ")
		return result
# есть возврат
# откатить ходы назад
def return_move(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, move):
	count_my = move[7:]
	#print(count_my)
	try:
		count_my = int(count_my)
	except:
		result = print_message(f"После return должно быть указано число ходов на которое хотим откатить. Попробуйте сделать ход вновь :")
		return play(result)
	if count_my > chet:
		result = print_message(f"В этой игре еще не было сделано {count_my} ходов. Попробуйте сделать ход вновь :")
		return play(result)
	chet = chet - count_my
	field = moves[chet]
	file_rewrite(chet, moves_file)
	print_field(field)
	result = print_message(f"Партия возвращена к {chet + 1} ходу. Сделайте следующий ход {color[chet % 2]}:")
	return chet, field, play(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, result)
# загрузка ходов из файла
def file_load(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, move):
	filemode = True
	chet = 0
	field = default_field
	file_name = move[5:]
	file_path = f"{os.path.dirname(os.path.abspath(__file__))}/{file_name}"
	if not os.path.exists(file_path):
		filemode = False
		result = print_message(f"Не нашли файла с наванием {file_name}. Попробуйте сделать ход вновь :")
		return play(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, result)
	with open(file_path, "r") as rfile:
		if os.path.getsize(file_path) == 0:
			result = print_message(f"Файл {file_name} пустой. Попробуйте сделать ход вновь :")
			return play(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, result)
		for line in rfile:
			dot = line.find('.')
			if dot != -1:
				line = line[dot + 1:]
			line = line.strip()
			lines_list  = line.split()
			for i in  lines_list:
				#print(i)
				i = i.replace('×', '-').replace('x', '-').replace(':', '-').replace('?', '').replace('!', '').replace('+', '')
				#i = i.translate(None, '!?#')
				if i[0].istitle():
					#print(i)
					i = i[1:]
				if len(i) > 5 and i[5] in ('#'):
					filemoves.append(i[:5])
					mate = True
				else:
					filemoves.append(i)
	#print(self.filemoves)
	#result = self.print_message(f"Партия была загружена из {file_name}")
	print(f"Файл с партией '{file_name}' был загружен.\n")
	return chet, field, filemode, play(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, 'start')
# добавить файл в список файлов
def file_read(chet, moves_file, file_name=dafault_file):
	with open(f"{os.path.dirname(os.path.abspath(__file__))}/{file_name}", "r") as rfile:
		rf = rfile.read()
		print(rf)
		#print(self.chet)
		moves_file[chet] = rf

# запись хода в файл
def file_save(chet, moves_file, move, file_name=dafault_file):
	with open(f"{os.path.dirname(os.path.abspath(__file__))}/{file_name}", "a") as wfile:
		if chet == 1:
			move = f"{1 + chet//2}. {move} "
		elif chet % 2 != 0:
			move = f"\n{1 + chet//2}. {move} "
		wfile.write(move)
	file_read(chet, moves_file)

# записать элемент списка в файл (перезапись)
def file_rewrite(chet, moves_file, file_name=dafault_file):
	#print(self.chet)
	with open(f"{os.path.dirname(os.path.abspath(__file__))}/{file_name}", "w") as wfile:
		wfile.write(moves_file[chet])

# остальные ходы
def play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves,  mate, move=None):
	# старт игры
	if chet == 0 and move == 'start':
		#print('here')
		move = start(chet, color, field, moves_file, filemode, filemoves)
	# откат ходов
	if move.find('return') == 0:
		chet, field, move = return_move(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, move)
	# из файла
	if move.find('file') == 0:
		chet, field, filemode, move = file_load(chet, color, field, lines, rows ,moves, moves_file, filemode, filemoves,  mate, move)
	# конец игры
	if move == 'end':
		#print("Партия была завершена")
		return 'end'
	# проверка формата сообщения от пользователя
	ch_f = check_format(move)
	if ch_f != 'ok':
		if filemode == True:
			print(ch_f)
			print(f"Была допущена ошибка в файле в блоке: {filemoves[chet]}. Игра будет прервана.")
			return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, 'end')
		else:
			result = print_message(ch_f)
			return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, result)
	list_field = field_to_list(field)
	cord_from, cord_to = move_to_cord(lines, rows, move)
	#checker = Checkers(list_field, cord_from, cord_to, chet)
	# проверки корректности хода
	ch_m = check_move(list_field, cord_from, cord_to, chet)
	if ch_m != 'ok':
		if filemode == True:
			print(ch_m)
			print(f"Была допущена ошибка в файле в блоке: {filemoves[chet]}. Игра будет прервана.")
			return play(chet, color, field, lines, rows ,moves_file, filemode, filemoves, moves, mate, 'end')
		else:
			result = print_message(ch_m)
			return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, result)
	
	# обновляем данные на поле
	moves[chet] = field
	figure_from = list_field[cord_from[0]][cord_from[1]]
	field = update_field(field, list_field=list_field, cord_from=cord_from, cord_to=cord_to)
	print_field(field)
	chet += 1
	if figure_from not in ['p', 'P', '.']:
		move = figure_from.upper() + move
	file_save(chet, moves_file, move)
	# начинаем следующий ход
	if filemode == True:
		if len(filemoves) == chet:
			print(f"Данные из файла кончились.")
			if mate == True:
				print(f"Партия закончилась матом.")
				return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, 'end')
			else:
				result = 'no'
		else:
			result = print_message(f"Переходим к следующему ходу(yes/no) или начнем игру?:")
		if result == 'yes':
			print(f"Ход из файла. Делаем ход {color[chet % 2]} по номером {chet + 1}: {filemoves[chet]}")
			return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, filemoves[chet])
		elif result == 'no':
			result = print_message(f"Сделайте следующий ход {color[chet % 2]} по номером {chet + 1}:")
			filemode = False
			return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, result)
		else:
			print(f"Вы ввели некорректное значение. Был Автоматически сделан следующий ход из файла: {filemoves[chet]}")
			return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, filemoves[chet])
	else:
		result = print_message(f"Сделайте следующий ход {color[chet % 2]} по номером {chet + 1}:")
		return play(chet, color, field, lines, rows , moves, moves_file, filemode, filemoves, mate, result)
		

#ch = Chess()
#print(ch.play('start'))
#print(ch.play('file bishop.txt'))
print(play(chet = 0, color = { 0: 'белыми', 1: 'черными'}, field = default_field, lines = ['1', '2', '3', '4', '5', '6', '7', '8'],
	rows = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'], moves = {}, moves_file = {}, filemode = False, filemoves = [], mate = False,  move='start'))