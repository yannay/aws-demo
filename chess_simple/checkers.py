lines = ['1', '2', '3', '4', '5', '6', '7', '8']
rows = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

# проверка формата хода поданного на вход
def check_format(move: str):
	# lines = ['1', '2', '3', '4', '5', '6', '7', '8']
	# rows = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']
	if len(move) != 5:
		return f"Некоректный формат записи хода: {move}. Лишние символы в строке. Сделайте еще один ход: "
	if move.find('-') != 2 and move.find('—') != 2:
		return f"Некоректный формат записи хода: {move}. Не хватает тире. Сделайте еще один ход: "
	if move[0] not in rows or move[3] not in rows:
		return f"Некоректный формат записи хода: {move}. Буквенная часть не соответсвует ожиданиям. Сделайте еще один ход: "
	if move[1] not in lines or move[4] not in lines:
		return f"Некоректный формат записи хода: {move}. Цифровая часть не соответсвует ожиданиям. Сделайте еще один ход: "
	return 'ok'

#class Checkers():

# def __init__(self, list_field: list, cord_from: list, cord_to: list, chet: int):
# 	self.list_field = list_field
# 	self.cord_from = cord_from
# 	self.cord_to = cord_to
# 	self.chet = chet
# был ли ход по вертикали
def get_vert(list_field, cord_from, cord_to) -> bool:
	if cord_from[0] - cord_to[0] != 0:
		if cord_from[1] - cord_to[1] == 0:
			res = '.'
			for i in range(cord_from[0] + 1, cord_to[0]):
				field = list_field[i][cord_from[1]]
				if field != '.':
					res = field
			if res == '.':
				return True
			else: 
				raise ValueError(f"Некорректный ход. Поля между клетками хода долны быть пустыми. Сделайте еще один ход: ")
		else:
			return False
	else:
		return False

# был ли ход по горизонтали
def get_goriz(list_field, cord_from, cord_to) -> bool:
	if cord_from[1] - cord_to[1] != 0:
		#print(self.cord_from[0] - self.cord_to[0])
		if cord_from[0] - cord_to[0] == 0:
			res = '.'
			for i in range(cord_from[1] + 1, cord_to[1]):
				field = list_field[cord_from[0]][i]
				#print(field)
				if field != '.':
					res = field
			if res == '.':
				return True
			else: 
				raise ValueError(f"Некорректный ход. Поля между клетками хода долны быть пустыми. Сделайте еще один ход: ")
		else:
			return False
	else:
		return False

def __sign(num):
	return -1 if num < 0 else 1

# был ли ход по диагонали
def get_diag(list_field, cord_from, cord_to) -> bool:
	#print(self.cord_from[1] - self.cord_to[1])
	if cord_from[1] - cord_to[1] != 0:
		#print(self.cord_from[0] - self.cord_to[0])
		if abs(cord_from[0] - cord_to[0]) == abs(cord_from[1] - cord_to[1]):
			#print('here')
			res = '.'
			j = 0
			s0 = __sign(cord_to[0] - cord_from[0])
			s1 = __sign(cord_to[1] - cord_from[1])
			for i in range(abs(cord_from[1] - cord_to[1])):
				#print(i)
				#print(self.cord_from[0] + i * s0)
				if j == 0:
					j += 1
					continue
				field = list_field[cord_from[0] + i * s0][cord_from[1] + i * s1]
				#print(field)
				if field != '.':
					res = field
			if res == '.':
				return True
			else: 
				raise ValueError(f"Некорректный ход. Поля между клетками хода долны быть пустыми. Сделайте еще один ход: ")
			#return True
		else:
			return False
	else:
		return False

# проверка хода пешки
def check_pawn(list_field, cord_from, cord_to):
	if list_field[cord_from[0]][cord_from[1]] == 'P':
		chet = 1
	else:
		chet = -1
	#print(cord_from[0] - cord_to[0])
	#print(get_diag(cord_from, cord_to))
	# проверка когда пешка ест
	if list_field[cord_to[0]][cord_to[1]] != '.':
		assert get_diag(list_field, cord_from, cord_to) == True, (f"Некорректный ход пешкой. Нарушена диагональ. Сделайте еще один ход: ")
		#print((cord_to[0] - cord_from[0]) * chet)
		#print((cord_to[1] - cord_from[1]) * chet)
		assert (cord_to[0] - cord_from[0]) * chet == 1, (f"Некорректный ход пешкой. Неправильное расстояние хода. Сделайте еще один ход: ")
		assert abs(cord_to[1] - cord_from[1]) == 1, (f"Некорректный ход пешкой. Неправильное расстояние хода. Сделайте еще один ход: ")
	# проверка когда пешка не ест
	elif list_field[cord_to[0]][cord_to[1]] == '.':
		assert get_vert(list_field, cord_from, cord_to) == True, (f"Некорректный ход пешкой. Нарушена вертикаль. Сделайте еще один ход: ")
		if cord_from[0] == 1 or cord_from[0] == 6:
			assert (cord_to[0] - cord_from[0]) * chet <= 2, f"Некорректный ход пешкой. Неправильное расстояние хода. Сделайте еще один ход: "
		else:
			assert (cord_to[0] - cord_from[0]) * chet == 1, f"Некорректный ход пешкой. Неправильное расстояние хода. Сделайте еще один ход: " 
	return 'ok'

def check_rook(list_field, cord_from, cord_to):
	assert get_diag(list_field, cord_from, cord_to) == False, (f"Некорректный ход ладьей. Не может ходить по диагонали. Сделайте еще один ход: ")
	assert get_vert(list_field, cord_from, cord_to) == True or get_goriz(list_field, cord_from, cord_to) == True, (f"Некорректный ход ладьей. Нарушена горизонталь или вертикаль. Сделайте еще один ход: ")

def check_bishop(list_field, cord_from, cord_to):
	assert get_vert(list_field, cord_from, cord_to) == False, (f"Некорректный ход слоном. Нарушена вертикаль. Сделайте еще один ход: ")
	assert get_goriz(list_field, cord_from, cord_to) == False, (f"Некорректный ход слоном. Нарушена горизонталь. Сделайте еще один ход: ")
	assert get_diag(list_field, cord_from, cord_to) == True, (f"Некорректный ход слоном. Нарушена диагональ. Сделайте еще один ход: ")

def check_queen(list_field, cord_from, cord_to):
	v = get_vert(list_field, cord_from, cord_to)
	g = get_goriz(list_field, cord_from, cord_to)
	d = get_diag(list_field, cord_from, cord_to)
	if v == False and g == False:
		assert d == True, (f"Некорректный ход ферзем. Нарушена диагональ. Сделайте еще один ход: ")
	elif v == True:
		assert g == False, (f"Некорректный ход ферзем. Нарушена горизонталь. Сделайте еще один ход: ")
	elif g == True:
		assert v == False, (f"Некорректный ход ферзем. Нарушена вертикаль. Сделайте еще один ход: ")

def check_king(list_field, cord_from, cord_to):
	assert abs(cord_to[0] - cord_from[0]) <= 1, f"Некорректный ход королем. Неправильное расстояние хода. Сделайте еще один ход: "
	assert abs(cord_to[1] - cord_from[1]) <= 1, f"Некорректный ход королем. Неправильное расстояние хода. Сделайте еще один ход: "
	v = get_vert(list_field, cord_from, cord_to)
	g = get_goriz(list_field, cord_from, cord_to)
	d = get_diag(list_field, cord_from, cord_to)
	if v == False and g == False:
		assert d == True, f"Некорректный ход королем. Нарушена диагональ. Сделайте еще один ход: "
	elif v == True:
		assert g == False, f"Некорректный ход королем. Нарушена горизонталь. Сделайте еще один ход: "
	elif g == True:
		assert v == False, f"Некорректный ход королем. Нарушена вертикаль. Сделайте еще один ход: "
	
def check_knight(cord_from, cord_to):
	vert = cord_to[0] - cord_from[0]
	goriz = cord_to[1] - cord_from[1]
	if abs(vert) == 2:
		assert abs(goriz) == 1, f"Некорректный ход конем. Нужно ходить под углом. Сделайте еще один ход: "
	elif abs(vert) == 1:
		assert abs(goriz) == 2, f"Некорректный ход конем. Нужно ходить под углом. Сделайте еще один ход: "
	else:
		raise ValueError(f"Некорректный ход конем. Нужно ходить под углом. Сделайте еще один ход: ")

# проверяем какой цвет ходит
def check_order(figure_from, chet):
	#figure_from = self.list_field[self.cord_from[0]][self.cord_from[1]]
	#print(figure_from)
	#print(self.chet)
	if chet % 2 == 0:
		assert figure_from.istitle(), f"Сейчас должны ходить белые. Сделайте еще один ход: "
	else:
		assert not figure_from.istitle(), f"Сейчас должны ходить черные. Сделайте еще один ход: "
	
# общая проверка хода
def check_move(list_field, cord_from, cord_to, chet):
	try:
		#print(self.list_field)
		#print(self.cord_from)
		#print(self.cord_to)
		figure_from = list_field[cord_from[0]][cord_from[1]]
		figure_to = list_field[cord_to[0]][cord_to[1]]
		check_order(figure_from, chet)
		if figure_from == '.':
			return f"Нельзя ходить с пустого поля. Сделайте еще один ход: "
		elif figure_from == 'p' or figure_from == 'P':
			check_pawn(list_field, cord_from, cord_to)
		elif figure_from == 'r' or figure_from == 'R':
			check_rook(list_field, cord_from, cord_to)
		elif figure_from == 'b' or figure_from == 'B':
			check_bishop(list_field, cord_from, cord_to)
		elif figure_from == 'q' or figure_from == 'Q':
			check_queen(list_field, cord_from, cord_to)
		elif figure_from == 'k' or figure_from == 'K':
			check_king(list_field, cord_from, cord_to)
		elif figure_from == 'n' or figure_from == 'N':
			check_knight(cord_from, cord_to)						
		#print(figure_to.istitle())
		# проверка что едим чужую фигуру
		if figure_to.istitle():
			#print('title')
			assert not figure_from.istitle(), f"Нельзя есть свою фигуру. Сделайте еще один ход: "
		elif not figure_to.istitle() and figure_to != '.':
			#print('not title')
			assert figure_from.istitle(), f"Нельзя есть свою фигуру. Сделайте еще один ход: "
		#print(figure_from)
		#print(figure_to)
	except (ValueError, AssertionError) as e:
		#print(e)
		return e
	return 'ok'	 

			

